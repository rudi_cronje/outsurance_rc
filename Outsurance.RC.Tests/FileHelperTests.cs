﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Outsurance.RC;
using Outsurance.RC.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outsurance.RC.Tests
{
    [TestClass()]
    public class FileHelperTests
    {

        [TestMethod()]
        public void ImportCustomerFileTest()
        {

            var importPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Import", "data.csv");

            FileHelper fh = new FileHelper();
            var resActual = fh.ImportCustomerFile(importPath);

            var resExpected = new List<Customer>();
            resExpected.Add(new Customer() { FirstName = "Jimmy", LastName = "Smith", Address = "102 Long Lane", PhoneNumber = "29384857" });
            resExpected.Add(new Customer() { FirstName = "Clive", LastName = "Owen", Address = "65 Ambling Way", PhoneNumber = "31214788" });

            CollectionAssert.Equals(resExpected, resActual);


        }


        [TestMethod()]
        public void GetNameFrequencyTest()
        {
            var objCustomerList = new List<Customer>();
            objCustomerList.Add(new Customer() { FirstName = "Matt", LastName = "Brown" });
            objCustomerList.Add(new Customer() { FirstName = "Heinrich", LastName = "Jones" });
            objCustomerList.Add(new Customer() { FirstName = "Johnson", LastName = "Smith" });
            objCustomerList.Add(new Customer() { FirstName = "Tim", LastName = "Johnson" });

            FileHelper fh = new FileHelper();
            var resActual = fh.GetNameFrequency(objCustomerList);


            Dictionary<string, int> resExpected = new Dictionary<string, int>();
            resExpected.Add("Johnson", 2);
            resExpected.Add("Brown", 1);
            resExpected.Add("Heinrich", 1);
            resExpected.Add("Jones", 1);
            resExpected.Add("Matt", 1);
            resExpected.Add("Smith", 1);
            resExpected.Add("Tim", 1);

            CollectionAssert.AreEqual(resExpected, resActual);

        }

        [TestMethod()]
        public void GetAddressesByStreetNameTest()
        {
            var objCustomerList = new List<Customer>();
            objCustomerList.Add(new Customer() { Address = "4 CCC Street" });
            objCustomerList.Add(new Customer() { Address = "1 FFF Street" });
            objCustomerList.Add(new Customer() { Address = "6 AAA Street" });
            objCustomerList.Add(new Customer() { Address = "3 DDD Street" });
            objCustomerList.Add(new Customer() { Address = "5 BBB Street" });
            objCustomerList.Add(new Customer() { Address = "2 EEE Street" });

            FileHelper fh = new FileHelper();
            var resActual = fh.GetAddressesByStreetName(objCustomerList);

            List<string> resExpected = new List<string>();
            resExpected.Add("6 AAA Street");
            resExpected.Add("5 BBB Street");
            resExpected.Add("4 CCC Street");
            resExpected.Add("3 DDD Street");
            resExpected.Add("2 EEE Street");
            resExpected.Add("1 FFF Street");

            CollectionAssert.AreEqual(resExpected, resActual);
        }

        [TestMethod()]
        public void ExportResultsTest()
        {
            var exportPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Export", "testFile.txt");
            var resExpected = "This is a test string";

            FileHelper fh = new FileHelper();
            var exportResult = fh.ExportResults(resExpected, exportPath);

            string resActual = "";

            if (exportResult)
            {
                resActual = File.ReadAllText(exportPath);
            }

            Assert.AreEqual(resExpected, resActual);
        }
            
    }
}