﻿using Outsurance.RC.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outsurance.RC
{
    class Program
    {
        static void Main(string[] args)
        {

            string importPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Import", "data.csv");
            string exportPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Export");

            FileHelper fh = new FileHelper();
            var objList = fh.ImportCustomerFile(importPath);
                        
            var res1 = fh.GetNameFrequency(objList);
            var res2 = fh.GetAddressesByStreetName(objList);
            
            fh.ExportResults(res1.ConvertToString(), Path.Combine(exportPath, "File1.txt"));
            fh.ExportResults(res2.ConvertToString(), Path.Combine(exportPath, "File2.txt"));

        }
    }
}
