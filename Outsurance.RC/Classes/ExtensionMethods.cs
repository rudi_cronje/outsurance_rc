﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outsurance.RC.Classes
{
    public static class ExtensionMethods
    {

        public static string ConvertToString(this Dictionary<string, int> objList)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in objList)
            {
                sb.AppendLine(String.Format("{0}, {1}", item.Key, item.Value));
            }

            return sb.ToString();
            
        }

        public static string ConvertToString(this List<string> objList)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in objList)
            {
                sb.AppendLine(item);
            }

            return sb.ToString();

        }

    }
}
