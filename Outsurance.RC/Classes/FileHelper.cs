﻿using CsvHelper;
using Outsurance.RC.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Outsurance.RC.Classes
{
    public class FileHelper
    {

        public FileHelper()
        {
            
        }

        public List<Customer> ImportCustomerFile(string path)
        {

            TextReader reader = File.OpenText(path);

            var csv = new CsvReader(reader);
            var objCustomerList = csv.GetRecords<Customer>().ToList();

            return objCustomerList;

        }

        public Dictionary<string, int> GetNameFrequency(List<Customer> objCustomerList)
        {

            var q = objCustomerList
                .Select(t => new[] { t.FirstName, t.LastName })
                .SelectMany(i => i)
                .GroupBy(n => n)
                .Select(n => new
                           {
                               Name = n.Key,
                               Count = n.Count()
                           }
                        )    
                .OrderByDescending(n => n.Count)
                .ThenBy(n => n.Name)
                .ToDictionary(t => t.Name, t => t.Count);

            return q;   
        }

        public List<string> GetAddressesByStreetName(List<Customer> objCustomerList)
        {

            var q = (from obj in objCustomerList

                     let @Street = obj.Address.Split(' ')[1]

                     select new
                     {
                         FullAddress = obj.Address,
                         Street = @Street
                     }).OrderBy(x => x.Street)
                     .Select(x => x.FullAddress).ToList();

            return q;

        }

        public bool ExportResults(string strContent, string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
                        
            try
            {
                File.WriteAllText(path, strContent);
                return true;
            }
            catch
            {
                return false;
            }
            
        }
    }
}
